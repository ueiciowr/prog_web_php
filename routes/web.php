<?php

use App\Http\Controllers\ImcController;
use App\Http\Controllers\InssController;
use App\Http\Controllers\JurosController;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\MediaController;
use App\Http\Controllers\MaiorController;
use App\Http\Controllers\Parcelamento;
use App\Http\Controllers\TrocoCedulasController;
use App\Http\Controllers\TrocoController;
use App\Http\Controllers\TrocoMoedasController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::resource('media', MediaController::class);
Route::resource('maior', MaiorController::class);
Route::resource('parcelamento', Parcelamento::class);
Route::resource('juros', JurosController::class);
Route::resource('imc', ImcController::class);
Route::resource('troco', TrocoController::class);
Route::resource('trococedulas', TrocoCedulasController::class);
Route::resource('trocomoedas', TrocoMoedasController::class);
Route::resource('inss', InssController::class);
