<!DOCTYPE html>
<html>

<head>
    <title>Exercício 4 - Parcelamento com juros</title>
</head>

<body>
    <h2>Parcelamento com juros</h2>
    <hr>
    <form name="notas">
        Valor: <br>
        <input type="number" id="a" name="a" step="any" autofocus> <br>
        Parcelas:
        <br>
        <input type="number" id="b" name="b"> <br>
        <br>
        <input type="submit" value="Calcular">
        <?php
        $vp;
        $soma;
        $parcelas = "";
        $total;
        if (isset($_GET['a']) && $_GET['a'] != '') {
            $v = $_GET['a'];
            $p = $_GET['b'];
            if ($p > 0 && $p <= 5) {
                $vp = round($v / $p, 2);
                for ($i = 1; $i <= $p; $i++) {
                    $parcelas = "$parcelas \n<p>Parcela $i = R$ $vp</p>";
                }
                echo $parcelas;
                echo "Total = R$ $v";
            } else if ($p > 5 && $p <= 10) {
                $vp = $v / $p;
                $soma = 0;
                for ($i = 1; $i <= $p; $i++) {
                    $vp = round($vp * 1.02, 2);
                    $soma = round($soma + $vp, 2);
                    $parcelas = "$parcelas \n<p>Parcela $i = R$ $vp</p>\n";
                }
                echo $parcelas;
                echo "Total = R$ $soma";
            }
        }
        ?>
</body>

</html>
