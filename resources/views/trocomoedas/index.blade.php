<!DOCTYPE html>
<html>

<head>
    <title>Exercício 7 - Troco Moedas</title>
</head>

<body>
    <h2>Troco Moedas</h2>
    <hr>
    <form name="troco">
        Venda: <br><input type="number" step="any" name="v" id="v" autofocus> <br>
        Valor pago: <br><input type="number" step="any" name="p" id="p"> <br><br>
        <button type="submit">Calcular</button>
        <?php
        if (isset($_GET['v']) && $_GET['v'] != '') {
            $v = floor($_GET['v'] * 100) / 100;
            $p = floor($_GET['p'] * 100) / 100;
            $td = $p - $v;
            $n100 = floor($td / 100);
            $r = $td % 100;
            $n50 = floor($r / 50);
            $r = $r % 50;
            $n20 = floor($r / 20);
            $r = $r % 20;
            $n10 = floor($r / 10);
            $r = $r % 10;
            $n5 = floor($r / 5);
            $r = $r % 5;
            $n2 = floor($r / 2);
            $n1 = $r % 2;

            echo "<h2>CEDULAS:</h2>";
            echo "<p>Troco R$ $td";
            echo "<p>Notas de R$ 100: $n100</p>";
            echo "<p>Notas de R$ 50: $n50</p>";
            echo "<p>Notas de R$ 20: $n20</p>";
            echo "<p>Notas de R$ 10: $n10</p>";
            echo "<p>Notas de R$ 5: $n5</p>";
            echo "<p>Notas de R$ 1: $n1</p>";

            // Moedas
            $trocot = round($p - $v, 2);
            $td = floor($trocot);
            $tm = round(($trocot - $td) * 100, 2);

            $m50 = floor($tm / 50);
            $r = $tm % 50;
            $m25 = floor($r / 25);
            $r = $r % 25;
            $m10 = floor($r / 10);
            $r = $r % 10;
            $m5 = floor($r / 5);
            $m1 = $r % 5;

            echo "<h2>MOEDAS:</h2>";
            echo "<p>moeda de R$ 0,50: $m50</p>";
            echo "<p>moeda de R$ 0,25: $m25</p>";
            echo "<p>moeda de R$ 0,10: $m10</p>";
            echo "<p>moeda de R$ 0,05: $m5</p>";
            echo "<p>moeda de R$ 0,01: $m1</p>";
        } ?>
    </form>
</body>

</html>
