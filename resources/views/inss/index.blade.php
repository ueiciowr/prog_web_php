<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <!--Faça uma página web que leia o valor do salário bruto de um funcionário e calcule e escreva o valor do 
desconto do INSS e o valor do salário líquido. O INSS é descontado da folha de pagamento do trabalhador, 
sendo o valor descontado diretamente do salário do funcionário, antes de ele receber o valor líquido, com as 
alíquotas de acordo com a tabela válida a partir de janeiro de 2021:
SALÁRIO-DE-CONTRIBUIÇÃO (R$) ALÍQUOTA
até 1.100,00 7,5%
de 1.100,01 até 2.203,48 9%
de 2.203,49 até 3.305,22 12 %
de 3.305,23 até 6.433,57 14%
Acima de 6.433,57 isento -->

    <title>Salário descontado o INSS</title>
</head>

<body>
    <h2>Salário descontado o INSS</h2>
    <hr>
    <form>
        <label>Salário</label>
        <input type="text" name="salario" value="">
        <button type="submit">Enviar</button>
        <?php
        if (isset($_GET['salario'])) {
            $salario = $_GET['salario'];
            $desconto;
            $salario_liquido = 0;

            if ($salario <= 1100) {
                $desconto = $salario * 0.075;
            } else if ($salario >= 1100.01 && $salario <= 2203.48) {
                $desconto = (($salario - 1100) * 0.09) + 82.50;
            } else if ($salario >= 2203.48 && $salario <= 3305.22) {
                $desconto = (($salario - 2203.48) * 0.12) + 99.31 + 82.50;
            } else if ($salario >= 3305.23 && $salario <= 6433.57) {
                $desconto = (($salario - 3305.23) * 0.14) + 132.21 + 99.31 + 82.50;
            } else if ($salario > 6433.57) {
                $desconto = 751.99;
            }
            $salario_liquido = round($salario - $desconto, 2);
            $desc = round($desconto, 2);
            echo "<p>Salário liquido R$ $salario_liquido</p>";
            echo "<p>Desconto R$ $desc/mês</p>";
        }
        ?>
    </form>
</body>

</html>
