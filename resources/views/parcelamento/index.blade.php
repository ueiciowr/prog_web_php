<!DOCTYPE html>
<html>

<head>
  <title>Exercício 3 - Parcelamento</title>
</head>

<body>
  <h2>Parcelamento</h2>
  <hr>
  <form name="notas">
    Valor: <br>
    <input type="number" id="a" name="a" step="any" autofocus> <br>
    Parcelas:
    <br>
    <input type="number" name="b" id="b"> <br>
    <br>
    <input type="submit" value="Calcular">
    <br>
    <hr>
    <?php
    $vp;
    $parcelas = "";
    $total;
    if (isset($_GET['a']) && $_GET['a'] != '') {
      $v = $_GET['a'];
      $p = $_GET['b'];
      if ($p > 0 && $p <= 10) {
        $vp = round($v / $p, 2);
        for ($i = 1; $i <= $p; $i++) {
          $parcelas = "$parcelas <p>Parcela $i = R$ $vp</p>\n";
        }
        $total = "<p>Total R$ $v</p>";
        echo $parcelas;
        echo $total;
      }
    } else {
      $parcelas = "";
      $total = "";
    } ?>
  </form>
</body>
</html>
