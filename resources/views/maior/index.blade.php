<body>
    <h2>Maior número</h2>
    <form name="notas">
        1º Número: <input type="number" name="a" autofocus> <br>
        2º Número: <input type="number" name="b"><br>
        3º Número: <input type="number" name="c"><br>
        <?php
        if (isset($_GET['a']) && $_GET['a'] != '') {
            $a = $_GET['a'];
            $b = $_GET['b'];
            $c = $_GET['c'];
            $maior = max($a, $b, $c);
        } else {
            $maior = "";
        } ?>
        Maior: <input type="text" value="<?= $maior ?>" readonly> <br><br>
        <input type="submit" value="Calcular">
    </form>
    <hr>
    <?php
    if (isset($_GET['a'])) {
        echo "O maior valor é: $maior";
    }
    ?>
</body>
