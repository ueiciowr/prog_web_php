<body>
    <h2>Peso IMC</h2>
    <hr>
    <form name="peso">
        Altura: <br><input type="number" step="any" id="a" name="a" autofocus> <br>
        Peso: <br><input type="number" step="any" id="p" name="b"> <br>
        Sexo: <br>
        <input type="radio" id="f" value="f" name="sexo" checked>
        <label for="f">Feminino</label><br>
        <input type="radio" id="m" value="m" name="sexo">
        <label for="m">Masculino</label><br> <br>
        <input type="submit" value="Calcular">
        <?php
        if (isset($_GET['a']) && $_GET['a'] != '') {
            $a = $_GET['a'];
            $p = $_GET['b'];
            $pi;
            $sexo = $_GET['sexo'];
            if ($sexo == 'f') {
                $pi = round(((62.1 * $a) - 44.7), 2);
            }
            if ($sexo == 'm') {
                $pi = round(((72.7 * $a) - 58), 2);
            }
            if ($p >= $pi - 1 && $p <= $pi + 1) {
                echo "<p>Está no peso ideal!</p>";
            } else if ($p < $pi) {
                echo "Está abaixo do peso ideal, peso $pi Kg";
            } else if ($p > $pi) {
                echo "<p>Está acima do peso ideal, peso $pi Kg </p>";
            }
        } ?>
    </form>

</body>

</html>
