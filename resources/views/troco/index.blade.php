<!DOCTYPE html>
<html>

<head>
    <title>Exercício 6 - Troco</title>
</head>

<body>
    <h2>Troco</h2>
    <hr>
    <form name="troco">
        Venda: <br><input type="number" step="any" name="v" id="v" autofocus> <br>
        Valor pago: <br><input type="number" step="any" name="p" id="p"> <br><br>
        <button type="submit">Calcular</button>
        <?php
        if (isset($_GET['v']) && $_GET['v'] != '') {
            $v = $_GET['v'];
            $p = $_GET['p'];
            $troco = round($p - $v, 2);
            echo "<p>$troco</p>";
        } ?>
    </form>
</body>

</html>
